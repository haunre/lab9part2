/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vegetable.Sheridan.Henry;

/**
 *
 * @author 123
 */
public class VegetableFactory {
    
    private static VegetableFactory factory;

    private VegetableFactory() { 
    }
    
    public static VegetableFactory getInstance(){ 
    {
        if (factory==null)
            factory = new VegetableFactory();
    }
        return factory;
    }
   
    public Vegetable getVegetable(String color, double size){
        if(color=="orange" && size>=1.5){
            return new Carrot(color, size);
        }
        
        if(color=="green" && size<=1){
            return new Carrot(color, size);
        }
        if(color=="pink" && size>=1.4){
            return new Beet(color, size);  
    }
        
        if(color=="pink" && size<=1){
            return new Beet(color, size);  
    }
        return null;
    }
     
    


